#!/bin/sh

wget https://repo.unityx.org/unityx.key
apt-key add unityx.key && rm -f unityx.key
echo 'deb https://repo.unityx.org/main testing main' | sudo tee /etc/apt/sources.list.d/unity-x.list
sudo apt-get update

add-apt-repository -y universe
add-apt-repository -y multiverse

apt-get purge -y ubuntu-session gnome-initial-setup gnome-control-center gnome-terminal nautilus --auto-remove

apt-get install -y unityx gdm3

apt-get purge -y gnome-session ubuntu-session
